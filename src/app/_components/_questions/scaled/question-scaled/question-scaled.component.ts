import {Component, Input, OnInit} from "@angular/core";
import {QuestionScaled} from "../../../../_models/question-scaled";

@Component({
    selector: "app-question-scaled",
    templateUrl: "./question-scaled.component.html",
    styleUrls: ["./question-scaled.component.scss"],
})
export class QuestionScaledComponent implements OnInit {
    @Input() question: QuestionScaled;

    Arr = Array;

    public constructor() {
    }

    public ngOnInit() {
    }

}
