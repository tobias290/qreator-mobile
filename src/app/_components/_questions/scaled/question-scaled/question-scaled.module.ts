import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { IonicModule } from '@ionic/angular';

import {QuestionScaledComponent} from "./question-scaled.component";


@NgModule({
    imports: [
        CommonModule,
        IonicModule,
    ],
    declarations: [QuestionScaledComponent],
    exports: [QuestionScaledComponent]
})
export class QuestionScaledComponentModule {}
