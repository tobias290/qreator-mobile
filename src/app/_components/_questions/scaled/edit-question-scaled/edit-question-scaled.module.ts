import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { IonicModule } from '@ionic/angular';
import {EditQuestionScaledComponent} from "./edit-question-scaled.component";
import {ReactiveFormsModule} from "@angular/forms";


@NgModule({
    imports: [
        CommonModule,
        IonicModule,
        ReactiveFormsModule
    ],
    declarations: [EditQuestionScaledComponent],
    exports: [EditQuestionScaledComponent]
})
export class EditQuestionScaledComponentModule {}
