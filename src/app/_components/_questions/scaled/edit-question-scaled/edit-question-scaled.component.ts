import {Component, EventEmitter, Input, OnInit, Output} from "@angular/core";
import {QuestionScaled} from "../../../../_models/question-scaled";
import {FormControl, Validators} from "@angular/forms";
import {ApiService} from "../../../../api.service";
import {URLS} from "../../../../urls";
import {AlertController} from "@ionic/angular";

@Component({
    selector: "app-edit-question-scaled",
    templateUrl: "./edit-question-scaled.component.html",
    styleUrls: ["./edit-question-scaled.component.scss"],
    providers: [ApiService]
})
export class EditQuestionScaledComponent implements OnInit {
    @Input() question: QuestionScaled;

    @Output() onSave = new EventEmitter();

    questionName = new FormControl("", Validators.required);
    questionMin = new FormControl("", Validators.required);
    questionMax = new FormControl("", Validators.required);
    questionInterval = new FormControl("", Validators.required);
    isRequired: boolean;

    public constructor(private apiService: ApiService, private alertController: AlertController) {
    }

    public ngOnInit() {
        this.questionName.setValue(this.question.name);
        this.questionMin.setValue(this.question.min);
        this.questionMax.setValue(this.question.max);
        this.questionInterval.setValue(this.question.interval);
        this.isRequired = this.question.isRequired;
    }

    /**
     * Saves the changed made to the question.
     */
    public save() {
        let data = {};

        // Only send data is changed are made

        if (this.questionName.value !== this.question.name)
            data["name"] = this.questionName.value;

        if (this.isRequired !== this.question.isRequired)
            data["is_required"] = this.isRequired;

        if (this.questionMin.value !== this.question.min)
            data["min"] = this.questionMin.value;

        if (this.questionMax.value !== this.question.max)
            data["max"] = this.questionMax.value;

        if (this.questionInterval.value !== this.question.interval)
            data["interval"] = this.questionInterval.value;

        this.apiService
            .patch(
                `${URLS.PATCH.QUESTION.editScaled}/${this.question.id}`,
                data,
                ApiService.createTokenHeader(sessionStorage.getItem("token")),
            )
            .subscribe(success => this.onSave.emit(), error => console.log(error));
    }

    /**
     * Deletes the question.
     */
    public delete() {
        this.alertController.create({
            header: "Delete Question!",
            message: "Are you sure you want to delete this question?",
            buttons: [
                {
                    text: "Delete",
                    cssClass: "test",
                    handler: () => {
                        this.apiService
                            .delete(
                                `${URLS.DELETE.QUESTION.deleteOpen}/${this.question.id}`,
                                ApiService.createTokenHeader(sessionStorage.getItem("token")),
                            )
                            .subscribe(success => this.onSave.emit(), error => console.log(error));
                    }
                }, {
                    text: "Cancel",
                    role: "cancel",
                }
            ]
        }).then((res) => {
            res.present();
        });
    }
}
