import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { IonicModule } from '@ionic/angular';
import {ResponseQuestionScaledComponent} from "./response-question-scaled.component";
import {ChartsModule} from "ng2-charts";


@NgModule({
    imports: [
        CommonModule,
        IonicModule,
        ChartsModule,
    ],
    declarations: [ResponseQuestionScaledComponent],
    exports: [ResponseQuestionScaledComponent]
})
export class ResponseQuestionScaledComponentModule {}
