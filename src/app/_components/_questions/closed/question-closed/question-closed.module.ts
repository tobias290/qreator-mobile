import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { IonicModule } from '@ionic/angular';
import {QuestionClosedComponent} from "./question-closed.component";

@NgModule({
    imports: [
        CommonModule,
        IonicModule,
    ],
    declarations: [QuestionClosedComponent],
    exports: [QuestionClosedComponent]
})
export class QuestionClosedComponentModule {}
