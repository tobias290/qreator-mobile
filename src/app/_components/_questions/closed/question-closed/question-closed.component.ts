import {Component, Input, OnChanges, OnInit} from "@angular/core";
import {QuestionClosed} from "../../../../_models/question-closed";
import {QuestionClosedOption} from "../../../../_models/question-closed-option";
import {ApiService} from "../../../../api.service";
import {URLS} from "../../../../urls";

@Component({
    selector: "app-question-closed",
    templateUrl: "./question-closed.component.html",
    styleUrls: ["./question-closed.component.scss"],
    providers: [ApiService]
})
export class QuestionClosedComponent implements OnChanges {
    @Input() question: QuestionClosed;

    existingOptions: QuestionClosedOption[] = [];

    public constructor(private apiService: ApiService) {
    }

    public ngOnChanges() {
        this.apiService.get(
            URLS.GET.QUESTION_OPTION.questionClosedOptions(this.question.id),
            ApiService.createTokenHeader(sessionStorage.getItem("token"))
        )
            .subscribe(success => {
                // @ts-ignore
                for (let option of success.success.options) {
                    this.existingOptions.push(new QuestionClosedOption(option));
                }
            }, error => console.log(error))
    }

}
