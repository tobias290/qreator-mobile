import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { IonicModule } from '@ionic/angular';
import {EditQuestionClosedComponent} from "./edit-question-closed.component";
import {ReactiveFormsModule} from "@angular/forms";


@NgModule({
    imports: [
        CommonModule,
        IonicModule,
        ReactiveFormsModule
    ],
    declarations: [EditQuestionClosedComponent],
    exports: [EditQuestionClosedComponent]
})
export class EditQuestionClosedComponentModule {}
