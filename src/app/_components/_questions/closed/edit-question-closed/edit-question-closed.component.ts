import {Component, EventEmitter, Input, OnInit, Output} from "@angular/core";
import {QuestionClosed} from "../../../../_models/question-closed";
import {ApiService} from "../../../../api.service";
import {AlertController} from "@ionic/angular";
import {FormControl, Validators} from "@angular/forms";
import {QuestionClosedOption} from "../../../../_models/question-closed-option";
import {URLS} from "../../../../urls";

interface OptionMeta {
    id: number|null,
    option: string,
}


@Component({
    selector: "app-edit-question-closed",
    templateUrl: "./edit-question-closed.component.html",
    styleUrls: ["./edit-question-closed.component.scss"],
    providers: [ApiService]
})
export class EditQuestionClosedComponent implements OnInit {
    @Input() question: QuestionClosed;

    @Output() onSave = new EventEmitter();

    questionName = new FormControl("", Validators.required);
    existingOptions: QuestionClosedOption[] = [];
    options: OptionMeta[] = [];
    deleteOptions: number[] = [];
    isRequired: boolean;

    public constructor(private apiService: ApiService, private alertController: AlertController) {
    }

    public ngOnInit() {
        this.questionName.setValue(this.question.name);
        this.isRequired = this.question.isRequired;

        this.apiService.get(
            URLS.GET.QUESTION_OPTION.questionClosedOptions(this.question.id),
            ApiService.createTokenHeader(sessionStorage.getItem("token"))
        )
            .subscribe(success => {
                // @ts-ignore
                for (let option of success.success.options) {
                    this.existingOptions.push(new QuestionClosedOption(option));
                    this.options.push({id: option.id, option: option.option});
                }
            }, error => console.log(error))
    }

    /**
     * Adds a new option.
     */
    public addOption() {
        this.options.push({id: null, option: "Untitled"});
    }

    /**
     * Deletes an existing option.
     *
     * @param index - Index of option to delete.
     */
    public deleteOption(index: number) {
        this.deleteOptions.push(this.options[index].id);
        this.options.splice(index, 1);
    }

    /**
     * Saves the changed made to the question.
     */
    public save() {
        /**
         * Deletes an options that are to be deleted.
         * Once complete it refreshes the view.
         */
        let deleteOptions = () => this.deleteOptions.forEach((optionId, i) => {
            this.apiService
                .delete(
                    `${URLS.DELETE.QUESTION_OPTION.deleteOption}/${optionId}`,
                    ApiService.createTokenHeader(sessionStorage.getItem("token")),
                ).subscribe(success => {
                if (this.options.length - 1 === i) {
                    this.onSave.emit();
                }
            }, error => console.log(error));
        });

        /**
         * Adds new options.
         * Once complete it calls the calls function to delete any options.
         */
        let addAndEditNewOptions = () => this.options.forEach((option, i) => {
            option.id !== null ?
                // Edit option as it already has an id
                this.apiService
                    .patch(
                        `${URLS.PATCH.QUESTION_OPTION.editOption}/${option.id}`,
                        {...option, question_closed_id: this.question.id},
                        ApiService.createTokenHeader(sessionStorage.getItem("token")),
                    ).subscribe(success => {
                    if (this.options.length - 1 === i) {
                        this.deleteOptions.length !== 0 ? deleteOptions() : this.onSave.emit();
                    }
                }, error => console.log(error))
                :
                // Add new option as it has no id
                this.apiService
                    .post(
                        `${URLS.POST.QUESTION_OPTION.addOption}`,
                        {...option, question_closed_id: this.question.id},
                        ApiService.createTokenHeader(sessionStorage.getItem("token")),
                    ).subscribe(success => {
                    if (this.options.length - 1 === i) {
                        this.deleteOptions.length !== 0 ? deleteOptions() : this.onSave.emit();
                    }
                }, error => console.log(error));
        });

        let data = {};

        // Only send data if changed are made

        if (this.questionName.value !== this.question.name) {
            data["name"] = this.questionName.value;
        }

        if (this.isRequired !== this.question.isRequired) {
            data["is_required"] = this.isRequired;
        }

        // Makes any changes to the question
        // Once complete it calls the function to add any new options ot the question
        this.apiService
            .patch(`${URLS.PATCH.QUESTION.editClosed}/${this.question.id}`,
                data,
                ApiService.createTokenHeader(sessionStorage.getItem("token")),
            ).subscribe(success => {
            if (this.options.length !== 0) {
                addAndEditNewOptions();
            } else if (this.deleteOptions.length !== 0) {
                deleteOptions();
            } else {
                this.onSave.emit();
            }
        }, error => console.log(error));
    }

    /**
     * Deletes the question.
     */
    public delete() {
        this.alertController.create({
            header: "Delete Question!",
            message: "Are you sure you want to delete this question?",
            buttons: [
                {
                    text: "Delete",
                    cssClass: "test",
                    handler: () => {
                        this.apiService
                            .delete(
                                `${URLS.DELETE.QUESTION.deleteOpen}/${this.question.id}`,
                                ApiService.createTokenHeader(sessionStorage.getItem("token")),
                            )
                            .subscribe(success => this.onSave.emit(), error => console.log(error));
                    }
                }, {
                    text: "Cancel",
                    role: "cancel",
                }
            ]
        }).then((res) => {
            res.present();
        });
    }
}
