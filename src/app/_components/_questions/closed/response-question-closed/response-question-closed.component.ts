import {Component, Input, OnInit} from "@angular/core";
import {QuestionOpen} from "../../../../_models/question-open";
import {QuestionClosedOption} from "../../../../_models/question-closed-option";
import {ChartOptions, ChartType} from "chart.js";
import {Label, SingleDataSet} from "ng2-charts";

@Component({
    selector: "app-response-question-closed",
    templateUrl: "./response-question-closed.component.html",
    styleUrls: ["./response-question-closed.component.scss"],
})
export class ResponseQuestionClosedComponent implements OnInit {
    @Input() question: QuestionOpen;
    @Input() options: QuestionClosedOption[];

    isTable: boolean = false;
    hideNoResponses: boolean = false;

    chartOptions: ChartOptions = {
        responsive: true,
    };
    chartLabels: Label[] = [];
    chartData: SingleDataSet = [];
    chartType: ChartType = "pie";
    chartLegend = true;

    public ngOnInit() {
        let labels = [];
        let data = [];

        this.options.forEach(option => {
            labels.push(option.option);
            data.push(option.responses);

            this.chartLabels = labels;
            this.chartData = data;
        });
    }

    /**
     * Changes the chart type.
     *
     * @param {string} type - Updates the type of graph used to show the data.
     */
    changeChartType(type) {
        this.isTable = false;

        if (type === "pie") {
            this.chartType = "pie";

            this.chartOptions = {
                responsive: true,
            };
            this.chartLegend = true;
        } else if (type === "bar") {
            this.chartType = "bar";
            this.chartOptions = {
                responsive: true,
                scales: {
                    yAxes: [{
                        ticks: {
                            beginAtZero: true
                        },
                        scaleLabel: {
                            display: true,
                            labelString: "Responses"
                        }
                    }],
                    xAxes: [{
                        scaleLabel: {
                            display: true,
                            labelString: "Options"
                        }
                    }],
                }
            };
            this.chartLegend = false;
        } else if (type === "horizontalBar") {
            this.chartType = "horizontalBar";
            this.chartOptions = {
                responsive: true,
                scales: {
                    yAxes: [{
                        scaleLabel: {
                            display: true,
                            labelString: "Options"
                        }
                    }],
                    xAxes: [{
                        ticks: {
                            beginAtZero: true
                        },
                        scaleLabel: {
                            display: true,
                            labelString: "Responses"
                        }
                    }],
                }
            };
            this.chartLegend = false;
        } else if (type === "table") {
            this.isTable = true;
            this.hideNoResponses = false;
        }
    }
}
