import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { IonicModule } from '@ionic/angular';
import {ChartsModule} from "ng2-charts";
import {ResponseQuestionClosedComponent} from "./response-question-closed.component";


@NgModule({
    imports: [
        CommonModule,
        IonicModule,
        ChartsModule,
    ],
    declarations: [ResponseQuestionClosedComponent],
    exports: [ResponseQuestionClosedComponent]
})
export class ResponseQuestionClosedComponentModule {}
