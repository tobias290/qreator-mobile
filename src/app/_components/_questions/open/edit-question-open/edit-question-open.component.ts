import {Component, EventEmitter, Input, OnInit, Output} from "@angular/core";
import {QuestionOpen} from "../../../../_models/question-open";
import {FormControl, Validators} from "@angular/forms";
import {ApiService} from "../../../../api.service";
import {URLS} from "../../../../urls";
import {AlertController} from "@ionic/angular";

@Component({
    selector: "app-edit-question-open",
    templateUrl: "./edit-question-open.component.html",
    styleUrls: ["./edit-question-open.component.scss"],
    providers: [ApiService]
})
export class EditQuestionOpenComponent implements OnInit {
    @Input() question: QuestionOpen;

    @Output() onSave = new EventEmitter();

    questionName = new FormControl("", Validators.required);
    isRequired: boolean;

    public constructor(private apiService: ApiService, private alertController: AlertController) {
    }

    public ngOnInit() {
        this.questionName.setValue(this.question.name);
        this.isRequired = this.question.isRequired;
    }

    /**
     * Saves the changed made to the question.
     */
    public save() {
        let data = {};

        // Only send data is changed are made

        if (this.questionName.value !== this.question.name) {
            data["name"] = this.questionName.value;
        }

        if (this.isRequired !== this.question.isRequired) {
            data["is_required"] = this.isRequired;
        }

        this.apiService
            .patch(
                `${URLS.PATCH.QUESTION.editOpen}/${this.question.id}`,
                data,
                ApiService.createTokenHeader(sessionStorage.getItem("token")),
            )
            .subscribe(success => this.onSave.emit(), error => console.log(error));
    }

    /**
     * Deletes the question.
     */
    public delete() {
        this.alertController.create({
            header: "Delete Question!",
            message: "Are you sure you want to delete this question?",
            buttons: [
                {
                    text: "Delete",
                    handler: () => {
                        this.apiService
                            .delete(
                                `${URLS.DELETE.QUESTION.deleteOpen}/${this.question.id}`,
                                ApiService.createTokenHeader(sessionStorage.getItem("token")),
                            )
                            .subscribe(success => this.onSave.emit(), error => console.log(error));
                    }
                }, {
                    text: "Cancel",
                    role: "cancel",
                }
            ]
        }).then((res) => {
            res.present();
        });
    }
}
