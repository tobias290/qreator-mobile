import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { IonicModule } from '@ionic/angular';
import {EditQuestionOpenComponent} from "./edit-question-open.component";
import {ReactiveFormsModule} from "@angular/forms";


@NgModule({
    imports: [
        CommonModule,
        IonicModule,
        ReactiveFormsModule
    ],
    declarations: [EditQuestionOpenComponent],
    exports: [EditQuestionOpenComponent]
})
export class EditQuestionOpenComponentModule {}
