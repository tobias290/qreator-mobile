import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { IonicModule } from '@ionic/angular';
import {QuestionOpenComponent} from "./question-open.component";


@NgModule({
    imports: [
        CommonModule,
        IonicModule,
    ],
    declarations: [QuestionOpenComponent],
    exports: [QuestionOpenComponent]
})
export class QuestionOpenComponentModule {}
