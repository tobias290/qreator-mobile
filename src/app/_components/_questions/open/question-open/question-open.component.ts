import {Component, Input, OnInit} from "@angular/core";
import {QuestionOpen} from "../../../../_models/question-open";

@Component({
    selector: "app-question-open",
    templateUrl: "./question-open.component.html",
    styleUrls: ["./question-open.component.scss"],
})
export class QuestionOpenComponent implements OnInit {
    @Input() question: QuestionOpen;

    public constructor() {
    }

    public ngOnInit() {
    }

}
