import {Component, Input} from "@angular/core";
import {QuestionOpen} from "../../../../_models/question-open";
import {QuestionOpenResponse} from "../../../../_models/question-open-response";

@Component({
    selector: "app-response-question-open",
    templateUrl: "./response-question-open.component.html",
    styleUrls: ["./response-question-open.component.scss"],
})
export class ResponseQuestionOpenComponent {
    @Input() question: QuestionOpen;
    @Input() responses: QuestionOpenResponse[];
}
