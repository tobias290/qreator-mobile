import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { IonicModule } from '@ionic/angular';
import {ResponseQuestionOpenComponent} from "./response-question-open.component";


@NgModule({
    imports: [
        CommonModule,
        IonicModule,
    ],
    declarations: [ResponseQuestionOpenComponent],
    exports: [ResponseQuestionOpenComponent]
})
export class ResponseQuestionOpenComponentModule {}
