import {NgModule} from "@angular/core";
import {CommonModule} from "@angular/common";
import {FormsModule} from "@angular/forms";
import {Routes, RouterModule} from "@angular/router";

import {IonicModule} from "@ionic/angular";

import {QuestionnaireDetailsPage} from "./questionnaire-details.page";

const routes: Routes = [
    {
        path: "", component: QuestionnaireDetailsPage
    },
];

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        IonicModule,
        RouterModule.forChild(routes),
        Clipboard
    ],
    declarations: [QuestionnaireDetailsPage],
    exports: [QuestionnaireDetailsPage],
})
export class QuestionnaireDetailsPageModule {
}
