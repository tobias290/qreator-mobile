import {Component, OnInit} from "@angular/core";
import { Clipboard } from '@ionic-native/clipboard/ngx';
import {ActivatedRoute, ParamMap, Router} from "@angular/router";
import {ApiService} from "../api.service";
import {Questionnaire} from "../_models/questionnaire";
import {URLS} from "../urls";
import {AlertController, LoadingController, ModalController, ToastController} from "@ionic/angular";
import {CreateQuestionnairePage} from "../create-questionnaire/create-questionnaire.page";

@Component({
    selector: "app-questionnaire-details",
    templateUrl: "./questionnaire-details.page.html",
    styleUrls: ["./questionnaire-details.page.scss"],
    providers: [ApiService, Clipboard],
})
export class QuestionnaireDetailsPage implements OnInit {
    loading: Boolean = true;
    questionnaire: Questionnaire;

    constructor(
        private apiService: ApiService,
        private route: ActivatedRoute,
        private router: Router,
        private loadingController: LoadingController,
        private toastController: ToastController,
        private clipboard: Clipboard,
        private alertController: AlertController,
        private modalController: ModalController
    ) {
    }

    ngOnInit() {
        this.loadingController.create({
            message: "Loading Questionnaire",
        }).then((res) => {
            res.present();
        });

        this.apiService
            .get(
                `${URLS.GET.QUESTIONNAIRE.get}/${this.route.snapshot.paramMap.get("id")}`,
                ApiService.createTokenHeader(sessionStorage.getItem("token"))
            )
            .subscribe(
                success => {
                    this.questionnaire = new Questionnaire(success);
                    this.loading = false;
                    this.loadingController.dismiss();
                },
                error => console.log(error)
            );
    }

    /**
     * Refreshes the content.
     *
     * @param event
     */
    public refresh(event) {
        this.apiService
            .get(
                `${URLS.GET.QUESTIONNAIRE.get}/${this.route.snapshot.paramMap.get("id")}`,
                ApiService.createTokenHeader(sessionStorage.getItem("token"))
            )
            .subscribe(
                success => {
                    this.questionnaire = new Questionnaire(success);
                    event.target.complete();
                },
                error => console.log(error)
            );
    }

    /**
     * Navigates to the editing questionnaire page.
     */
    public editQuestionnairePage() {
        this.router.navigateByUrl(`home/tabs/home/${this.questionnaire.id}/edit`);
    }

    /**
     * Copies a link to the public questionnaires page.
     */
    public sendQuestionnaire() {
        this.clipboard.copy(`http://192.168.1.114:8000/frontend/dist/frontend/#/public/questionnaires/${this.questionnaire.id}/answer`);

        this.toastController.create({
            message: "Questionnaire Link Copied",
            color: "primary",
            duration: 1000
        }).then((res) => {
            res.present();
        })
    }

    /**
     * Presents the create questionnaire modal.
     */
    public editQuestionnaire() {
        this.modalController.create({
            component: CreateQuestionnairePage,
            componentProps: {
                "questionnaire": this.questionnaire
            }
            //cssClass: "modal",
        }).then((res) => {
            res.present();

            res.onDidDismiss().then((dis) => this.ngOnInit());
        });
    }

    /**
     * Navigates to the questionnaire responses page.
     */
    public questionnaireResponses() {
        this.router.navigateByUrl(`home/tabs/home/${this.questionnaire.id}/responses`);
    }

    /**
     * Deletes the questionnaire.
     */
    public delete() {
        this.alertController.create({
            header: "Delete Questionnaire!",
            message: "Are you sure you want to delete this questionnaire?",
            buttons: [
                {
                    text: "Delete",
                    cssClass: "test",
                    handler: () => {
                        this.apiService
                            .delete(
                                `${URLS.DELETE.QUESTIONNAIRE.delete}/${this.questionnaire.id}`,
                                ApiService.createTokenHeader(sessionStorage.getItem("token"))
                            )
                            .subscribe(
                                success => this.router.navigateByUrl("/home/tabs/home"),
                                error => console.log(error),
                            );
                    }
                }, {
                    text: "Cancel",
                    role: "cancel",
                }
            ]
        }).then((res) => {
            res.present();
        });
    }
}
