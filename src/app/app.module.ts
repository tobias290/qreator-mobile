import {NgModule} from "@angular/core";
import {BrowserModule} from "@angular/platform-browser";
import {RouteReuseStrategy} from "@angular/router";

import {IonicModule, IonicRouteStrategy} from "@ionic/angular";
import {SplashScreen} from "@ionic-native/splash-screen/ngx";
import {StatusBar} from "@ionic-native/status-bar/ngx";

import {AppRoutingModule} from "./app-routing.module";
import {AppComponent} from "./app.component";
import {HttpClientModule} from "@angular/common/http";
import {EditQuestionPage} from "./edit-question/edit-question.page";
import {EditQuestionOpenComponentModule} from "./_components/_questions/open/edit-question-open/edit-question-open.module";
import {EditQuestionClosedComponentModule} from "./_components/_questions/closed/edit-question-closed/edit-question-closed.module";
import {EditQuestionScaledComponentModule} from "./_components/_questions/scaled/edit-question-scaled/edit-question-scaled.module";
import {CreateQuestionnairePage} from "./create-questionnaire/create-questionnaire.page";
import {ReactiveFormsModule} from "@angular/forms";

@NgModule({
    declarations: [AppComponent, EditQuestionPage, CreateQuestionnairePage],
    entryComponents: [EditQuestionPage, CreateQuestionnairePage],
    imports: [BrowserModule, IonicModule.forRoot({
        swipeBackEnabled: true,
        scrollAssist: false
    }), AppRoutingModule, HttpClientModule, EditQuestionOpenComponentModule, EditQuestionClosedComponentModule, EditQuestionScaledComponentModule, ReactiveFormsModule],
    providers: [
        StatusBar,
        SplashScreen,
        {provide: RouteReuseStrategy, useClass: IonicRouteStrategy},
    ],
    bootstrap: [AppComponent]
})
export class AppModule {
}
