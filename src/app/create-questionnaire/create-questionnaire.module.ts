import {NgModule} from "@angular/core";
import {CommonModule} from "@angular/common";
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import {Routes, RouterModule} from "@angular/router";

import {IonicModule} from "@ionic/angular";

import {CreateQuestionnairePage} from "./create-questionnaire.page";

const routes: Routes = [
    {
        path: "",
        component: CreateQuestionnairePage
    }
];

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        IonicModule,
        RouterModule.forChild(routes),
        ReactiveFormsModule
    ],
    declarations: [CreateQuestionnairePage]
})
export class CreateQuestionnairePageModule {
}
