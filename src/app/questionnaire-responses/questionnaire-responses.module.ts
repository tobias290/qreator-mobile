import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { QuestionnaireResponsesPage } from './questionnaire-responses.page';
import {ResponseQuestionOpenComponentModule} from "../_components/_questions/open/response-question-open/response-question-open.module";
import {ResponseQuestionScaledComponentModule} from "../_components/_questions/scaled/response-question-scaled/response-question-scaled.module";

const routes: Routes = [
  {
    path: '',
    component: QuestionnaireResponsesPage
  }
];

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        IonicModule,
        RouterModule.forChild(routes),
        ResponseQuestionOpenComponentModule,
        ResponseQuestionScaledComponentModule
    ],
  declarations: [QuestionnaireResponsesPage]
})
export class QuestionnaireResponsesPageModule {}
