import {Component, OnInit} from "@angular/core";
import {Questionnaire} from "../_models/questionnaire";
import {Question} from "../_models/question";
import {ApiService} from "../api.service";
import {ActivatedRoute, Router} from "@angular/router";
import {QuestionOpen} from "../_models/question-open";
import {QuestionOpenResponse} from "../_models/question-open-response";
import {QuestionClosed} from "../_models/question-closed";
import {QuestionClosedOption} from "../_models/question-closed-option";
import {QuestionScaled} from "../_models/question-scaled";
import {QuestionScaledResponse} from "../_models/question-scaled-response";
import {URLS} from "../urls";

interface DataMetadata {
    //user: User,
    questionnaire: Questionnaire,
    questions: Question[],
    questionClosedOptions: Object,
    openQuestionResponses: Object,
    scaledQuestionResponses: Object,
}

@Component({
    selector: "app-questionnaire-responses",
    templateUrl: "./questionnaire-responses.page.html",
    styleUrls: ["./questionnaire-responses.page.scss"],
    providers: [ApiService],
})
export class QuestionnaireResponsesPage implements OnInit {
    loading = true;

    data: DataMetadata = {
        //user: null,
        questionnaire: null,
        questions: [],
        questionClosedOptions: [],
        openQuestionResponses: [],
        scaledQuestionResponses: [],
    };

    public constructor(private apiService: ApiService, private router: Router, private route: ActivatedRoute) {
    }

    public ngOnInit() {
        if (!sessionStorage.getItem("token")) {
            this.router.navigateByUrl("/login");
            return;
        }

          this.apiService
              .get(URLS.GET.RESPONSES.questionnaireResponses(this.route.snapshot.paramMap.get("id")), ApiService.createTokenHeader(sessionStorage.getItem("token")))
              .subscribe(success => {
                  // @ts-ignore
                  this.data.questionnaire = new Questionnaire(success.success.questionnaire);

                  // @ts-ignore
                  // Add all the open questions
                  for (let openQuestion of success.success.questionnaire.open_questions || []) {
                      this.data.questions.push(new QuestionOpen(openQuestion));

                      // Empty array for the question responses
                      let responses: QuestionOpenResponse[] = [];

                      // Push each response to the options array after instantiating the correct model
                      openQuestion.responses.forEach(response => responses.push(new QuestionOpenResponse(response)));

                      // Add the list of responses with the key being the question ID
                      this.data.openQuestionResponses[openQuestion.id] = responses;
                  }

                  // @ts-ignore
                  // Add all the closed questions
                  for (let closedQuestion of success.success.questionnaire.closed_questions || []) {
                      this.data.questions.push(new QuestionClosed(closedQuestion));

                      // Empty array for the question options
                      let options: QuestionClosedOption[] = [];

                      // Push each option to the options array after instantiating the correct model
                      closedQuestion.options.forEach(option => options.push(new QuestionClosedOption(option)));

                      // Add the list of options with the key being the question ID
                      this.data.questionClosedOptions[closedQuestion.id] = options;
                  }

                  // @ts-ignore
                  // Add all the scaled questions
                  for (let scaledQuestion of success.success.questionnaire.scaled_questions || []) {
                      this.data.questions.push(new QuestionScaled(scaledQuestion));

                      // Empty array for the question responses
                      let responses: QuestionScaledResponse[] = [];

                      // Push each response to the options array after instantiating the correct model
                      scaledQuestion.responses.forEach(response => responses.push(new QuestionScaledResponse(response)));

                      // Add the list of responses with the key being the question ID
                      this.data.scaledQuestionResponses[scaledQuestion.id] = responses;
                  }

                  // Sort questions into the order they should be in the questionnaire.
                  this.data.questions.sort((a, b) => a.position - b.position);

                  this.loading = false;
              }, error => console.log(error));
    }

    /**
     * Determines whether the given question is an instance of the given type.
     *
     * @param {QuestionOpen|QuestionClosed|QuestionScaled} instance - Instance of question.
     * @param {string} type - Type to check instance against.
     */
    public isQuestionType(instance, type) {
        switch (type) {
            case "open":
                return instance instanceof QuestionOpen;
            case "closed":
                return instance instanceof QuestionClosed;
            case "scaled":
                return instance instanceof QuestionScaled;
            default:
                return false;
        }
    }

}
