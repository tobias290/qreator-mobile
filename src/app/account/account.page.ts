import {Component, OnInit} from "@angular/core";
import {User} from "../_models/user";
import {Settings} from "../_models/settings";
import {ApiService} from "../api.service";
import {URLS} from "../urls";
import {Router} from "@angular/router";

@Component({
    selector: "app-account",
    templateUrl: "./account.page.html",
    styleUrls: ["./account.page.scss"],
    providers: [ApiService]
})
export class AccountPage implements OnInit {
    loading = true;

    forms = {
        showChangeNameForm: false,
        showChangEmailForm: false,
        showChangePasswordForm: false,
        showDeleteAccountForm: false,
    };

    user: User;
    settings: Settings;

    appToggleSwitchStartState: boolean = false;
    emailToggleSwitchState: boolean = false;

    public constructor(private apiService: ApiService, private router: Router) {
    }

    public ngOnInit() {
        this.getUserDetails();
    }

    /**
     * Gets the user's details
     */
    public getUserDetails() {
        this.loading = true;

        this.apiService
            .get(URLS.GET.USER.details, ApiService.createTokenHeader(sessionStorage.getItem("token")))
            .subscribe(success => {
                this.user = new User(success);

                this.getUserSettings();
            }, error => console.log(error));
    }

    /**
     * Changes one of the user's settings.
     */
    public getUserSettings() {
        this.apiService
            .get(URLS.GET.USER.settings, ApiService.createTokenHeader(sessionStorage.getItem("token")))
            .subscribe(success => {
                this.settings = new Settings(success);

                this.appToggleSwitchStartState = this.settings.enableInAppNotifications;
                this.emailToggleSwitchState = this.settings.enableEmailNotifications;

                this.loading = false;
            }, error => console.log(error));
    }

    public changeSettings(setting) {
        this.apiService
            .patch(URLS.PATCH.USER.editSettings, setting, ApiService.createTokenHeader(sessionStorage.getItem("token")))
            .subscribe(success => this.getUserDetails(), error => console.log(error));
    }

    /**
     * Refreshed the data.
     *
     * @param event
     */
    public refresh(event) {
        this.apiService
            .get(URLS.GET.USER.details, ApiService.createTokenHeader(sessionStorage.getItem("token")))
            .subscribe(success => {
                this.user = new User(success);

                this.apiService
                    .get(URLS.GET.USER.settings, ApiService.createTokenHeader(sessionStorage.getItem("token")))
                    .subscribe(success => {
                        this.settings = new Settings(success);

                        this.appToggleSwitchStartState = this.settings.enableInAppNotifications;
                        this.emailToggleSwitchState = this.settings.enableEmailNotifications;

                        this.loading = false;

                        event.target.complete();
                    }, error => console.log(error));
            }, error => console.log(error));
    }

    /**
     * Sign outs the user.
     */
    public signOut() {
        this.apiService
            .get(URLS.GET.USER.signOut, ApiService.createTokenHeader(sessionStorage.getItem("token")))
            .subscribe(res => {
                if (res["success"]) {
                    sessionStorage.removeItem("token");
                    this.router.navigateByUrl("/login");
                }
            }, error => console.log(error));
    }
}
