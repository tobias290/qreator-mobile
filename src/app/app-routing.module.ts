import {NgModule} from "@angular/core";
import {PreloadAllModules, RouterModule, Routes} from "@angular/router";

const routes: Routes = [
    {
        path: "",
        redirectTo: "login",
        pathMatch: "full"
    },
    {
        path: "home",
        loadChildren: () => import("./tabs/tabs.module").then(m => m.TabsPageModule)
    },
    {path: "login", loadChildren: "./login/login.module#LoginPageModule"},
    {path: "home", loadChildren: "./home/home.module#HomePageModule"},
  { path: 'edit-question', loadChildren: './edit-question/edit-question.module#EditQuestionPageModule' },
  { path: 'create-questionnaire', loadChildren: './create-questionnaire/create-questionnaire.module#CreateQuestionnairePageModule' },
  { path: 'account', loadChildren: './account/account.module#AccountPageModule' },
  { path: 'notifications', loadChildren: './notifications/notifications.module#NotificationsPageModule' },
  { path: 'questionnaire-responses', loadChildren: './questionnaire-responses/questionnaire-responses.module#QuestionnaireResponsesPageModule' },
];

@NgModule({
    imports: [
        RouterModule.forRoot(routes, {preloadingStrategy: PreloadAllModules})
    ],
    exports: [RouterModule]
})
export class AppRoutingModule {
}
