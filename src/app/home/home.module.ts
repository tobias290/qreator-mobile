import {NgModule} from "@angular/core";
import {CommonModule} from "@angular/common";
import {FormsModule} from "@angular/forms";
import {Routes, RouterModule} from "@angular/router";

import {IonicModule} from "@ionic/angular";

import {HomePage} from "./home.page";
import {TitleComponentModule} from "../_components/title/title.module";
import {QuestionnaireDetailsPage} from "../questionnaire-details/questionnaire-details.page";
import {EditQuestionnairePage} from "../edit-questionnaire/edit-questionnaire.page";
import {QuestionClosedComponentModule} from "../_components/_questions/closed/question-closed/question-closed.module";
import {QuestionOpenComponentModule} from "../_components/_questions/open/question-open/question-open.module";
import {QuestionScaledComponentModule} from "../_components/_questions/scaled/question-scaled/question-scaled.module";
import {QuestionnaireResponsesPage} from "../questionnaire-responses/questionnaire-responses.page";
import {ResponseQuestionOpenComponentModule} from "../_components/_questions/open/response-question-open/response-question-open.module";
import {ResponseQuestionScaledComponentModule} from "../_components/_questions/scaled/response-question-scaled/response-question-scaled.module";
import {ResponseQuestionClosedComponentModule} from "../_components/_questions/closed/response-question-closed/response-question-closed.module";

const routes: Routes = [
    {path: "", component: HomePage},
    {path: ":id", component: QuestionnaireDetailsPage},
    {path: ":id/edit", component: EditQuestionnairePage},
    {path: ":id/responses", component: QuestionnaireResponsesPage}
];

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        IonicModule,
        RouterModule.forChild(routes),
        TitleComponentModule,
        QuestionClosedComponentModule,
        QuestionOpenComponentModule,
        QuestionClosedComponentModule,
        QuestionScaledComponentModule,
        ResponseQuestionOpenComponentModule,
        ResponseQuestionScaledComponentModule,
        ResponseQuestionClosedComponentModule,
    ],
    declarations: [HomePage, QuestionnaireDetailsPage, EditQuestionnairePage, QuestionnaireResponsesPage]
})
export class HomePageModule {

}
