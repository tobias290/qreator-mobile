import {Component, OnInit} from "@angular/core";
import {ApiService} from "../api.service";
import {URLS} from "../urls";
import {Questionnaire} from "../_models/questionnaire";
import {Router} from "@angular/router";
import {LoadingController} from "@ionic/angular";

@Component({
    selector: "app-home",
    templateUrl: "./home.page.html",
    styleUrls: ["./home.page.scss"],
    providers: [ApiService]
})
export class HomePage implements OnInit {
    loading = true;
    questionnaires: Questionnaire[];

    public constructor(private apiService: ApiService, public router: Router, private loadingController: LoadingController) {
    }

    public ngOnInit() {
        this.loadingController.create({
            message: "Loading Questionnaires",
        }).then((res) => {
            res.present();
        });

        this.apiService
            .get(URLS.GET.QUESTIONNAIRE.all, ApiService.createTokenHeader(sessionStorage.getItem("token")))
            .subscribe(res => {
                this.questionnaires = [];

                // @ts-ignore
                for (let questionnaire of res)
                    this.questionnaires.push(new Questionnaire(questionnaire));

                this.loading = false;
                this.loadingController.dismiss();
            }, error => console.log(error));
    }

    /**
     * Refreshes the content.
     *
     * @param event
     */
    public refresh(event) {
        this.apiService
            .get(URLS.GET.QUESTIONNAIRE.all, ApiService.createTokenHeader(sessionStorage.getItem("token")))
            .subscribe(res => {
                this.questionnaires = [];

                // @ts-ignore
                for (let questionnaire of res) {
                    this.questionnaires.push(new Questionnaire(questionnaire));
                }

                event.target.complete();
            }, error => console.log(error));
    }

    /**
     * Displays the action sheet with possible actions
     */
    public presentActionSheet() {
        console.log("Action Sheet");
    }
}
