import {Component, OnInit} from "@angular/core";
import {Clipboard} from "@ionic-native/clipboard/ngx";
import {Questionnaire} from "../_models/questionnaire";
import {Question} from "../_models/question";
import {ApiService} from "../api.service";
import {ActivatedRoute} from "@angular/router";
import {URLS} from "../urls";
import {QuestionOpen} from "../_models/question-open";
import {QuestionClosed} from "../_models/question-closed";
import {QuestionScaled} from "../_models/question-scaled";
import {ActionSheetController, LoadingController, ModalController, ToastController} from "@ionic/angular";
import {EditQuestionPage} from "../edit-question/edit-question.page";
import {CreateQuestionnairePage} from "../create-questionnaire/create-questionnaire.page";

interface DataMetadata {
    //user: User,
    questionnaire: Questionnaire,
    questions: Question[],
}

@Component({
    selector: "app-edit-questionnaire",
    templateUrl: "./edit-questionnaire.page.html",
    styleUrls: ["./edit-questionnaire.page.scss"],
    providers: [ApiService, Clipboard]
})
export class EditQuestionnairePage implements OnInit {
    loading = {
        //user: true,
        questionnaire: true,
        questions: true,
    };

    data: DataMetadata = {
        //user: null,
        questionnaire: null,
        questions: [],
    };

    public constructor(
        private apiService: ApiService,
        private route: ActivatedRoute,
        private modalController: ModalController,
        private loadingController: LoadingController,
        private toastController: ToastController,
        private clipboard: Clipboard,
        private actionSheetController: ActionSheetController
    ) {
    }

    public ngOnInit() {
        this.getQuestionnaire();
    }

    /**
     * Gets the questionnaire based of the ID parsed via the URL.
     */
    public getQuestionnaire() {
        // Get the questionnaire ID param from the route and get the requested questionnaire

        this.loadingController.create({
            message: "Loading Questions",
        }).then((res) => {
            res.present();
        });

        this.apiService
            .get(
                `${URLS.GET.QUESTIONNAIRE.get}/${this.route.snapshot.paramMap.get("id")}`,
                ApiService.createTokenHeader(sessionStorage.getItem("token"))
            )
            .subscribe(
                success => {
                    this.data.questionnaire = new Questionnaire(success);
                    this.loading.questionnaire = false;

                    this.getQuestions();
                },
                error => console.log(error)
            );

    }

    /**
     * Gets the questions for the given questionnaire.
     */
    public getQuestions() {
        this.data.questions = [];

        this.apiService
            .get(
                URLS.GET.QUESTION.questionnaireQuestions(this.data.questionnaire.id),
                ApiService.createTokenHeader(sessionStorage.getItem("token"))
            )
            .subscribe(
                success => {
                    // @ts-ignore
                    for (let openQuestion of success.success.open || []) {
                        this.data.questions.push(new QuestionOpen(openQuestion));
                    }

                    // @ts-ignore
                    for (let closedQuestion of success.success.closed || []) {
                        this.data.questions.push(new QuestionClosed(closedQuestion));
                    }

                    // @ts-ignore
                    for (let scaledQuestion of success.success.scaled || []) {
                        this.data.questions.push(new QuestionScaled(scaledQuestion));
                    }

                    // Sort questions into the order they should be in the questionnaire.
                    this.data.questions.sort((a, b) => a.position - b.position);
                    this.loading.questions = false;

                    this.loadingController.dismiss();
                },
                error => console.log(error)
            );
    }

    /**
     * Determines whether the given question is an instance of the given type.
     *
     * @param {QuestionOpen|QuestionClosed|QuestionScaled} instance - Instance of question.
     * @param {string} type - Type to check instance against.
     */
    public isQuestionType(instance, type) {
        switch (type) {
            case "open":
                return instance instanceof QuestionOpen;
            case "closed":
                return instance instanceof QuestionClosed;
            case "scaled":
                return instance instanceof QuestionScaled;
            default:
                return false;
        }
    }

    /**
     * Adds a question to the questionnaire.
     */
    public showAddQuestionActionSheet() {
        this.actionSheetController.create({
            header: "Add Question",
            mode: "md",
            buttons: [
                {
                    text: "Multiple Choice",
                    icon: "checkbox",
                    handler: () => this.addQuestion("closed", "radio")
                },
                {
                    text: "Check Boxes",
                    icon: "list",
                    handler: () => this.addQuestion("closed", "check")
                },
                {
                    text: "Drop Down",
                    icon: "arrow-dropdown-circle",
                    handler: () => this.addQuestion("closed", "drop_down")
                },
                {
                    text: "Single Line",
                    icon: "remove",
                    handler: () => this.addQuestion("open", "short")
                },
                {
                    text: "Paragraph",
                    icon: "menu",
                    handler: () => this.addQuestion("open", "long")
                },
                {
                    text: "Star Rating",
                    icon: "star",
                    handler: () => this.addQuestion("scaled", "star")
                },
                {
                    text: "Slider",
                    icon: "options",
                    handler: () => this.addQuestion("scaled", "slider")
                },
                {
                    text: "Cancel",
                    cssClass: "red",
                    role: "cancel",
                    handler: () => this.actionSheetController.dismiss()
                }
            ]
        }).then((res) => {
            res.present();
        });
    }

    /**
     * Adds a question to the questionnaire.
     *
     * @param {string} type - Question type (open, closed, scaled)
     * @param {string} subType - Question sub type.
     */
    public addQuestion(type, subType) {
        let url;
        let data;

        if (type === "open") {
            url = URLS.POST.QUESTION.addOpen;

            data = {
                position: this.data.questions.length + 1,
                is_long: subType === "long",
                questionnaire_id: this.data.questionnaire.id
            };
        } else if (type === "closed" || type === "scaled") {
            url = type === "closed" ? URLS.POST.QUESTION.addClosed : URLS.POST.QUESTION.addScaled;

            data = {
                position: this.data.questions.length + 1,
                type: subType,
                questionnaire_id: this.data.questionnaire.id
            };
        }

        this.apiService
            .post(url, data, ApiService.createTokenHeader(sessionStorage.getItem("token")))
            .subscribe(success => {
                // If a closed question was added, automatically give it one option
                if (type == "closed") {
                    this.apiService
                        .post(`${URLS.POST.QUESTION_OPTION.addOption}`,
                            // @ts-ignore
                            {option: "Option 1", question_closed_id: success.success.id},
                            ApiService.createTokenHeader(sessionStorage.getItem("token")))
                        .subscribe(success => this.getQuestionnaire());
                } else {
                    this.getQuestions()
                }
            }, error => console.log(error));
    }

    /**
     * Copies a link to the public questionnaires page.
     */
    public sendQuestionnaire() {
        this.clipboard.copy(`http://192.168.1.114:8000/frontend/dist/frontend/#/public/questionnaires/${this.data.questionnaire.id}/answer`);

        this.toastController.create({
            message: "Questionnaire Link Copied",
            color: "primary",
            duration: 1000
        }).then((res) => {
            res.present();
        });
    }

    /**
     *
     * @param {Question} question
     * @param {string} type
     */
    public loadQuestionEdit(question, type) {
        this.modalController.create({
            component: EditQuestionPage,
            //cssClass: "modal",
            componentProps: {
                question: question,
                type: type,
            }
        }).then((res) => {
            res.present();

            res.onDidDismiss().then((dis) => {
                if (dis.data.reload) {
                    this.getQuestionnaire();
                }
            });
        });
    }

    /**
     * Refreshes the content.
     *
     * @param event
     */
    public refresh(event) {
        this.data.questions = [];

        this.apiService
            .get(
                `${URLS.GET.QUESTIONNAIRE.get}/${this.route.snapshot.paramMap.get("id")}`,
                ApiService.createTokenHeader(sessionStorage.getItem("token"))
            )
            .subscribe(
                success => {
                    this.data.questionnaire = new Questionnaire(success);
                    this.loading.questionnaire = false;

                    this.apiService
                        .get(
                            URLS.GET.QUESTION.questionnaireQuestions(this.data.questionnaire.id),
                            ApiService.createTokenHeader(sessionStorage.getItem("token"))
                        )
                        .subscribe(
                            success => {
                                // @ts-ignore
                                for (let openQuestion of success.success.open || []) {
                                    this.data.questions.push(new QuestionOpen(openQuestion));
                                }

                                // @ts-ignore
                                for (let closedQuestion of success.success.closed || []) {
                                    this.data.questions.push(new QuestionClosed(closedQuestion));
                                }

                                // @ts-ignore
                                for (let scaledQuestion of success.success.scaled || []) {
                                    this.data.questions.push(new QuestionScaled(scaledQuestion));
                                }

                                // Sort questions into the order they should be in the questionnaire.
                                this.data.questions.sort((a, b) => a.position - b.position);
                                this.loading.questions = false;

                                this.loadingController.dismiss();
                            },
                            error => console.log(error)
                        );

                    event.target.complete();
                },
                error => console.log(error)
            );
    }
}
