import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EditQuestionnairePage } from './edit-questionnaire.page';

describe('EditQuestionnairePage', () => {
  let component: EditQuestionnairePage;
  let fixture: ComponentFixture<EditQuestionnairePage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EditQuestionnairePage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EditQuestionnairePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
