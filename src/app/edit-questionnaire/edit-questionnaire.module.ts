import {NgModule} from "@angular/core";
import {CommonModule} from "@angular/common";
import {FormsModule} from "@angular/forms";
import {Routes, RouterModule} from "@angular/router";

import {IonicModule} from "@ionic/angular";

import {EditQuestionnairePage} from "./edit-questionnaire.page";

const routes: Routes = [
    {
        path: "",
        component: EditQuestionnairePage
    }
];

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        IonicModule,
        RouterModule.forChild(routes),
        Clipboard
    ],
    declarations: [EditQuestionnairePage],
})
export class EditQuestionnairePageModule {
}
