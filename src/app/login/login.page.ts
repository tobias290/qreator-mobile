import {Component, OnInit} from "@angular/core";
import {FormControl, FormGroup, Validators} from "@angular/forms";
import {ApiService} from "../api.service";
import {Router} from "@angular/router";
import {URLS} from "../urls";
import {LoadingController} from "@ionic/angular";

@Component({
    selector: "app-login",
    templateUrl: "./login.page.html",
    styleUrls: ["./login.page.scss"],
    providers: [ApiService]
})
export class LoginPage implements OnInit {
    isServerError: boolean = false;
    serverErrorMessage: string;

    loginForm = new FormGroup({
        email: new FormControl("", Validators.required),
        password: new FormControl("", Validators.required),
    });

    public constructor(private apiService: ApiService, private router: Router, private loadingController: LoadingController) {
    }

    public ngOnInit() {
        // Automatically take the user to their dashboard if they are already logged in
        if (sessionStorage.getItem("token"))
            this.router.navigateByUrl("home");
    }

    /**
     * Returns a form control from the form group.
     *
     * @param {string} name - Name of form control
     *
     * @returns {FormControl} - Form group
     */
    public input(name: string) {
        return this.loginForm.get(name);
    }

    /**
     * Called when the sign up form is submitted
     */
    public onSubmit() {
        // Reset server errors
        this.isServerError = false;
        this.serverErrorMessage = null;

        // Form is not valid so do not submit
        if (!this.loginForm.valid) return;

        // Convert data to comply with API's format.
        let data = {
            // NOTE/TODO: Reset for production
            //email: this.loginForm.value.email,
            //password: this.loginForm.value.password,
            email: "tobiascompany@gmail.com",
            password: "pass1234",
        };

        this.presentLoading();

        this.apiService.post(URLS.POST.USER.login, data).subscribe(success => {
            this.loadingController.dismiss();
            this.success(success);

        }, (err) => {
            this.error(err)
        });
    }

    /**
     * Shows the loading popup.
     */
    private presentLoading() {
        this.loadingController.create({
            message: "Logging In",
        }).then((res) => {
            res.present();
        })
    }

    /**
     * Called if the request was successful.
     *
     * @param success - Success data returned by the response.
     */
    private success(success) {
        if (success.hasOwnProperty("success")) {
            sessionStorage.setItem("token", success.success.token);

            this.router.navigateByUrl("/home");
        }
    }

    /**
     * Called if an error occurred.
     *
     * @param error
     */
    private error(error) {
        this.isServerError = true;

        this.serverErrorMessage = error.error === undefined ? "Cannot connect to server" : error.error.message;
    }
}
