import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { EditQuestionPage } from './edit-question.page';
import {EditQuestionOpenComponentModule} from "../_components/_questions/open/edit-question-open/edit-question-open.module";
import {EditQuestionClosedComponentModule} from "../_components/_questions/closed/edit-question-closed/edit-question-closed.module";
import {EditQuestionScaledComponentModule} from "../_components/_questions/scaled/edit-question-scaled/edit-question-scaled.module";

const routes: Routes = [
  {
    path: '',
    component: EditQuestionPage
  }
];

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        IonicModule,
        RouterModule.forChild(routes),
        EditQuestionOpenComponentModule,
        EditQuestionClosedComponentModule,
        EditQuestionScaledComponentModule
    ],
  declarations: [EditQuestionPage]
})
export class EditQuestionPageModule {}
