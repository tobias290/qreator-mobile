import {Component, Input, OnInit, ViewChild} from "@angular/core";
import {ModalController} from "@ionic/angular";
import {Question} from "../_models/question";

@Component({
    selector: "app-edit-question",
    templateUrl: "./edit-question.page.html",
    styleUrls: ["./edit-question.page.scss"],
})
export class EditQuestionPage  {
    @Input() question: Question;
    @Input() type;

    @ViewChild("open", {static: false}) openQuestion;
    @ViewChild("closed", {static: false}) closedQuestion;
    @ViewChild("scaled", {static: false}) scaledQuestion;

    public constructor(private modalController: ModalController) {
    }

    /**
     * Saves the question.
     */
    public save() {
        if (this.type == "open") {
            this.openQuestion.save();
        } else if (this.type == "closed") {
            this.closedQuestion.save();
        } else if (this.type == "scaled") {
            this.scaledQuestion.save();
        }
    }

    public dismiss(reload) {
        this.modalController.dismiss({"reload": reload});
    }
}
