import {Component, OnInit} from "@angular/core";
import {URLS} from "../urls";
import {ApiService} from "../api.service";
import {User} from "../_models/user";
import {Notification} from "../_models/notification";
import {Router} from "@angular/router";
import {LoadingController} from "@ionic/angular";

@Component({
    selector: "app-notifications",
    templateUrl: "./notifications.page.html",
    styleUrls: ["./notifications.page.scss"],
    providers: [ApiService]
})
export class NotificationsPage implements OnInit {

    loading = false;

    notifications: Notification[] = [];

    public constructor(private apiService: ApiService, private router: Router, private loadingController: LoadingController,) {
    }

    ngOnInit() {
        if (!sessionStorage.getItem("token")) {
            this.router.navigateByUrl("/login");
            return;
        }

        this.getNotificationData();
    }

    /**
     * Gets the user's notifications.
     */
    getNotificationData() {
        this.loading = true;
        this.notifications = [];

        this.loadingController.create({
            message: "Loading Notifications",
        }).then((res) => {
            res.present();
        });

        this.apiService
            .get(URLS.GET.USER.notifications, ApiService.createTokenHeader(sessionStorage.getItem("token")))
            .subscribe(success => {
                // @ts-ignore
                for (let notification of success)
                    this.notifications.push(new Notification(notification));

                console.log(this.notifications[this.notifications.length - 1]);

                this.loadingController.dismiss();
                this.loading = false;
            }, error => console.log(error));
    }

    /**
     * Gets the user's notifications.
     */
    getNotificationDataOnRefresh(event) {
        this.notifications = [];

        this.apiService
            .get(URLS.GET.USER.notifications, ApiService.createTokenHeader(sessionStorage.getItem("token")))
            .subscribe(success => {
                // @ts-ignore
                for (let notification of success) {
                    this.notifications.push(new Notification(notification));
                }

                event.target.complete();
            }, error => console.log(error));
    }

    getNumberOfUnreadNotifications() {
        return this.notifications.filter(notification => !notification.isRead).length;
    }

    /**
     * Marks a notification as read.
     *
     * @param {string} id - Notification's ID.
     */
    readNotification(id) {
        this.apiService
            .patch(`${URLS.PATCH.USER.readNotification}/${id}`, {}, ApiService.createTokenHeader(sessionStorage.getItem("token")))
            .subscribe(success => {

                this.getNotificationData();
            }, error => console.log(error));
    }

    /**
     * Reads all of the notifications.
     */
    readAllNotifications() {
        this.apiService
            .patch(URLS.PATCH.USER.readAllNotifications, {}, ApiService.createTokenHeader(sessionStorage.getItem("token")))
            .subscribe(success => {
                this.getNotificationData();
            }, error => console.log(error));
    }

    /**
     * Deletes a notification.
     *
     * @param {string} id - Notification's ID.
     */
    deleteNotification(id) {
        this.apiService
            .delete(`${URLS.DELETE.USER.deleteNotification}/${id}`, ApiService.createTokenHeader(sessionStorage.getItem("token")))
            .subscribe(success => {
                this.getNotificationData();
            }, error => console.log(error));
    }

    /**
     * Deletes all of the notifications.
     */
    deleteAllNotifications() {
        this.apiService
            .delete(URLS.DELETE.USER.deleteAllNotifications, ApiService.createTokenHeader(sessionStorage.getItem("token")))
            .subscribe(success => {
                this.getNotificationData();
            }, error => console.log(error));
    }

}
