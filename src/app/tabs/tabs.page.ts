import {Component, OnInit} from "@angular/core";
import {ModalController} from "@ionic/angular";
import {CreateQuestionnairePage} from "../create-questionnaire/create-questionnaire.page";
import {ApiService} from "../api.service";
import {URLS} from "../urls";

@Component({
    selector: "app-tabs",
    templateUrl: "tabs.page.html",
    styleUrls: ["tabs.page.scss"],
    providers: [ApiService]
})
export class TabsPage implements OnInit {
    unreadNotificationsNumber: number = 0;

    public constructor(private apiService: ApiService, private modalController: ModalController) {
    }

    /**
     * Presents the create questionnaire modal.
     */
    public createQuestionnaire() {
        this.modalController.create({
            component: CreateQuestionnairePage,
            //cssClass: "modal",
        }).then((res) => {
            res.present();
        });
    }

    /**
     * Gets the user's notifications.
     */
    public ngOnInit() {
        this.apiService
            .get(URLS.GET.USER.notifications, ApiService.createTokenHeader(sessionStorage.getItem("token")))
            .subscribe(success => {
                // @ts-ignore
                for (let notification of success)
                    if (notification.read_at === null)
                        this.unreadNotificationsNumber++;
            }, error => console.log(error));
    }
}
