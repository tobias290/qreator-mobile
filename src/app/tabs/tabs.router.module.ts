import {NgModule} from "@angular/core";
import {RouterModule, Routes} from "@angular/router";
import {TabsPage} from "./tabs.page";
import {QuestionnaireDetailsPage} from "../questionnaire-details/questionnaire-details.page";
import {EditQuestionnairePage} from "../edit-questionnaire/edit-questionnaire.page";

const routes: Routes = [
    {
        path: "tabs",
        component: TabsPage,
        children: [
            {path: "home", children: [{path: "", loadChildren: () => import("../home/home.module").then(m => m.HomePageModule)}]},
            {path: "notifications", children: [{path: "", loadChildren: () => import("../notifications/notifications.module").then(m => m.NotificationsPageModule)}]},
            {path: "account", children: [{path: "", loadChildren: () => import("../account/account.module").then(m => m.AccountPageModule)}]},
            {path: "", redirectTo: "/home/tabs/home", pathMatch: "full"},
        ]
    },
    {path: "", redirectTo: "/home/tabs/home", pathMatch: "full"}
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class TabsPageRoutingModule {
}
